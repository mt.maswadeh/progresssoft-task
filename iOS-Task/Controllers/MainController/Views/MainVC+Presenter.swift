//
//  MainVC+Presenter.swift
//  iOS-Task
//
//  Created by Mohammad on 2/11/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

extension MainViewController : MainViewControllerPresenter {
    
    //MARK:- All Jobs

    func getAllJobsRequestSuccessful(_ jobList: [Job], _ query: String?) {

        self.allJobs = jobList
        self.presenter.searchGovJobsRequest(query: query)

    }
    
    func getAllJobsRequestFailed(_ error: String) {
        Dialogs.showError(error)

    }
    
     //MARK:- Github Jobs
    
    func githubJobsRequestSuccessful(_ jobList:[Job]){
        self.githubJobs = jobList
    }
    func githubJobsRequestFailed(_ error:String){
        Dialogs.showError(error)
    }
    
    //MARK:- Search Gov Jobs

    func searchGovJobsRequestSuccessful(_ jobList:[Job]){
        self.searchGovJobs = jobList
        self.allJobs = allJobs + jobList
        self.mainTableView.reloadData()
    }
    func searchGovJobsRequestFailed(_ error:String){
        Dialogs.showError(error)

    }

    
}

