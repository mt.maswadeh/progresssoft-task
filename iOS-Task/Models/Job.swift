//
//  Job.swift
//  iOS-Task
//
//  Created by Mohammad on 2/12/19.
//  Copyright © 2019 ProgressSoft. All rights reserved.
//

import Foundation

struct Job:Codable {
    
    // Github
    var type:String?
    var createdAt:String?
    var company:String?
    var companyUrl:String?
    var location:String?
    var title:String?
    var description:String?
    var howToApply:String?
    
    //Search.gov
    var positionTitle:String?
    var organizationName:String?
    var rateIntervalCode:String?
    var minimum:String?
    var maximum:String?
    var startDate:String?
    var endDate:String?
    var locations:[String]?
    
    
    // common
    var id:String?
    var url:String?
    var companyLogo:String?
    
    
    private enum CodingKeys : String, CodingKey {
        
        case type,company,location,title,description,minimum,maximum,locations,id,url
        
        // Github
        case createdAt = "created_at"
        case companyUrl = "company_url"
        case howToApply = "how_to_apply"
        
        //Search.gov
        case positionTitle = "position_title"
        case organizationName = "organization_name"
        case rateIntervalCode = "rate_interval_code"
        case startDate = "start_date"
        case endDate = "end_date"
        
        // common
        case companyLogo = "company_logo"
        
    }
}
